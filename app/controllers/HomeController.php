<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		return View::make('hello');
	}

	public function pertamax(){

		$args;
		$args['nama'] = "jacky";
		$args['kado'] = "BB - 8";
		echo "asdas";

		return View::make('testing', $args);
	}

	public function upload_file(){

	}

	public function form(){
		return View::make('form');
	}

	public function process_form(){
		$nama = Input::get('username');
		$pass = Input::get('pass');
		Session::set('username', $nama);

		echo Session::get('username');
	}

	public function cek_session(){
		echo Session::get('username');	
	}

	public function ebencot(){
		return View::make('ebencot');
	}

	public function logout(){
		Session::flush();
		return Redirect::to('/');
	}
}
