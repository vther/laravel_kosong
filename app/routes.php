<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', ['uses'=>'HomeController@pertamax']);
Route::get('/form', ['uses'=>'HomeController@form']);
Route::post('/form', ['uses'=>'HomeController@process_form']);

Route::get('/cek', ['uses'=>'HomeController@cek_session']);

Route::get('/logout', ['uses'=>'HomeController@logout']);

Route::get('/ebencot', ['uses'=>'HomeController@ebencot']);

#include('routes/jacky.php');
